﻿Imports System.IO
Imports System.Text
Module FileModule
    Public Sub AddText(fs As FileStream, value As String)
        Dim info As Byte() = New UTF8Encoding(True).GetBytes(value)
        fs.Write(info, 0, info.Length)
    End Sub
    Public Function AddTabs(count As Int16) As String
        Dim sb As New StringBuilder
        Dim i As Int16 = 0
        Do While i <= count
            sb.Append(vbTab)
            i += 1
        Loop
        Return sb.ToString
    End Function
End Module
