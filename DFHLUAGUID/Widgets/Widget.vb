﻿Imports System.Text
Public Class Widget
    Implements IFormattable
    Public name As String
    Protected parentname As String
    Public l As Short
    Public t As Short
    Public b As Short
    Public r As Short
    Public xalign As Short
    Public yalign As Short
    Public frame_background As String

    Public Overridable Function ToString(format As String, formatProvider As IFormatProvider) As String Implements IFormattable.ToString
        Dim sb As New StringBuilder
        Dim form As Integer
        Int16.TryParse(format, form)
        If Not (l.Equals(Nothing)) And Not (t.Equals(Nothing)) And Not (r.Equals(Nothing)) And Not (b.Equals(Nothing)) Then
            sb.AppendLine(AddTabs(form + 1) + "frame = {")
            sb.AppendLine(AddTabs(form + 2) + "l = " + l.ToString())
            sb.AppendLine(AddTabs(form + 2) + "t = " + t.ToString())
            sb.AppendLine(AddTabs(form + 2) + "r = " + r.ToString())
            sb.AppendLine(AddTabs(form + 2) + "b = " + b.ToString())
            If Not (xalign.Equals(Nothing)) Then
                sb.AppendLine(AddTabs(form + 2) + "xalign = " + xalign.ToString())
            End If
            If Not yalign.Equals(Nothing) Then
                sb.AppendLine(AddTabs(form + 2) + "yalign = " + yalign.ToString())
            End If
            sb.AppendLine(AddTabs(form + 1) + "}")
            Return sb.ToString()
        End If
        Return Nothing
    End Function
End Class
