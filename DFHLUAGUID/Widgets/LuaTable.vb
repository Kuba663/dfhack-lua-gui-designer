﻿Imports System.Dynamic
Imports System.Linq.Expressions
Imports System.Text

Public Class LuaTable
    Implements IDynamicMetaObjectProvider

    Public Function GetMetaObject(parameter As Expression) As DynamicMetaObject Implements IDynamicMetaObjectProvider.GetMetaObject
        Return New DynamicMetaTable(parameter, Me)
    End Function
    Private Class DynamicMetaTable
        Inherits DynamicMetaObject

        Friend Sub New(expression As Expression, value As LuaTable)
            MyBase.New(expression, BindingRestrictions.Empty, value)
        End Sub
        Public Overrides Function BindSetMember(binder As SetMemberBinder, value As DynamicMetaObject) As DynamicMetaObject
            Dim methodName As String = "SetDictionaryEntry"
            Dim restrictions As BindingRestrictions = BindingRestrictions.GetTypeRestriction(Expression, LimitType)
            Dim args As New List(Of Expression)(2)
            args.Insert(0, Expression.Constant(binder.Name))
            args.Insert(1, Expression.Convert(value.Expression, GetType(Object)))
            Dim self As Expression = Expression.Convert(Expression, LimitType)
            Dim methodCall As Expression = System.Linq.Expressions.Expression.Call(self, GetType(LuaTable).GetMethod(methodName), args)
            Dim setDictionaryEntry As New DynamicMetaObject(methodCall, restrictions)
            Return setDictionaryEntry
        End Function
        Public Overrides Function BindGetMember(binder As GetMemberBinder) As DynamicMetaObject
            Dim methodName As String = "GetDictionaryEntry"
            Dim args As List(Of Expression) = New List(Of Expression)()
            args.Add(Expression.Constant(binder.Name))
            Dim getDictionaryEntry As New DynamicMetaObject(System.Linq.Expressions.Expression.Call(System.Linq.Expressions.Expression.Convert(Expression, LimitType), GetType(LuaTable).GetMethod(methodName), args), BindingRestrictions.GetTypeRestriction(Expression, LimitType))
            Return getDictionaryEntry
        End Function
        Public Overrides Function BindInvokeMember(binder As InvokeMemberBinder, args() As DynamicMetaObject) As DynamicMetaObject
            Dim paramInfo As New StringBuilder()
            paramInfo.AppendFormat("Calling {0}(", binder.Name)
            For Each item In args
                paramInfo.AppendFormat("{0}, ", item.Value)
            Next
            paramInfo.Append(")")
            Dim parameters As New List(Of Expression)()
            parameters.Add(Expressions.Expression.Constant(paramInfo.ToString()))
            Dim methodInfo As New DynamicMetaObject(Expressions.Expression.Call(Expressions.Expression.Convert(Expression, LimitType), GetType(LuaTable).GetMethod("WriteMethodInfo"), parameters), BindingRestrictions.GetTypeRestriction(Expression, LimitType))
            Return methodInfo
        End Function
    End Class
    Dim storage As New Dictionary(Of String, Object)()
    Friend Sub SetDictionaryEntry(ByVal key As String, obj As Object)
        If storage.ContainsKey(key) Then
            storage.Item(key) = obj
        Else
            storage.Add(key, obj)
        End If
    End Sub
    Friend Function GetDictionaryEntry(ByVal key As String) As Object
        Dim result As Object = Nothing
        If storage.ContainsKey(key) Then
            result = storage.Item(key)
        End If
        Return result
    End Function
    Friend Function WriteMethodInfo(methodInfo As String)
        Return 666
    End Function
    Public Overrides Function ToString() As String
        Dim sb As New StringBuilder()
        sb.Append("{")
        For Each key In storage.Keys
            sb.AppendFormat("{0} = {1}, ", key, storage.Item(key).ToString())
        Next
        sb.Append("}")
        Return sb.ToString()
    End Function
End Class
