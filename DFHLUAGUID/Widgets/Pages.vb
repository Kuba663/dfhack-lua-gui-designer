﻿Imports System.Text
Public Class Pages
    Inherits Widget
    Implements IFormattable
    Public subviews As List(Of Widget)
    Public selected As String
    Public Overrides Function ToString(format As String, formatProvider As IFormatProvider) As String Implements IFormattable.ToString
        Dim sb As New StringBuilder
        Dim form As Integer
        Int16.TryParse(format, form)
        If form.Equals(0) Then
            sb.AppendLine("self:addviews{")
        End If
        sb.AppendLine(AddTabs(form) + "Pages{")
        sb.AppendLine(AddTabs(form + 1) + "view_id =" + "'" + MyBase.name + "',")
        sb.AppendLine(AddTabs(form + 1) + "subviews = {")
        Me.subviews.ForEach(New Action(Of Widget)(Sub(w As Widget) sb.Append(w.ToString(form + 2, formatProvider))))
        sb.AppendLine(AddTabs(form + 1) + "},")
        sb.AppendLine(AddTabs(form + 1) + "selected =" + Me.selected)
        If Not MyBase.ToString(form, formatProvider).Equals(Nothing) Then
            sb.Append(MyBase.ToString(form, formatProvider))
        End If
        sb.AppendLine(AddTabs(form) + "}")
        If form.Equals(0) Then
            sb.AppendLine("}")
        End If
        Return sb.ToString
    End Function
End Class
