﻿Imports System.Text
Public Class EditField
    Inherits Widget
    Implements IFormattable
    Public text_pen As String
    Public text As String
    Public on_char As String
    Public on_change As String
    Public on_submit As String
    Public key As String
    Public Overrides Function ToString(format As String, formatProvider As IFormatProvider) As String
        Dim sb As New StringBuilder
        Dim form As Integer
        Int16.TryParse(format, form)
        If form.Equals(0) Then
            sb.AppendLine("self:addviews{")
        End If
        sb.AppendLine(AddTabs(form) + "EditField{")
        sb.AppendLine(AddTabs(form + 1) + "text = " + Me.text)
        sb.AppendLine(AddTabs(form + 1) + "text_pen = " + Me.text_pen)
        If Me.on_char Then
            sb.AppendLine(AddTabs(form + 1) + "on_char = " + Me.on_char)
        End If
        If Me.on_change Then
            sb.AppendLine(AddTabs(form + 1) + "on_change = " + Me.on_change)
        End If
        sb.AppendLine(AddTabs(form + 1) + "on_submit = " + Me.on_submit)
        If Me.key Then
            sb.AppendLine(AddTabs(form + 1) + "key = " + Me.key)
        End If
        If Not MyBase.ToString(form, formatProvider).Equals(Nothing) Then
            sb.Append(MyBase.ToString(form, formatProvider))
        End If
        sb.AppendLine(AddTabs(form) + "}")
        If form.Equals(0) Then
            sb.AppendLine("}")
        End If
        Return sb.ToString
    End Function
End Class
