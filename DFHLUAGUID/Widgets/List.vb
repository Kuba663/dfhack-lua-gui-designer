﻿Imports System.Text

Public Class List
    Inherits Widget
    Public text_pen As String
    Public cursor_pen As String
    Public inactive_pen As String
    Public icon_pen As String
    Public on_select As String
    Public on_submit As String
    Public on_submit2 As String
    Public row_height As Short
    Public icon_width As Short
    Public scroll_keys As Table
    Public choices As New List(Of LuaTable)()
    Public Overrides Function ToString(format As String, formatProvider As IFormatProvider) As String
        Dim sb As New StringBuilder()
        Dim form As Integer
        Int16.TryParse(format, form)
        If form.Equals(0) Then
            sb.AppendLine("self:addviews{")
        End If
        sb.AppendLine(AddTabs(form) + "widgets.List{")
        sb.AppendLine(AddTabs(form + 1) + "text_pen = " + Me.text_pen + ",")
        sb.AppendLine(AddTabs(form + 1) + "cursor_pen = " + Me.cursor_pen + ",")
        sb.AppendLine(AddTabs(form + 1) + "inactive_pen = " + Me.inactive_pen + ",")
        sb.AppendLine(AddTabs(form + 1) + "icon_pen = " + Me.icon_pen + ",")
        sb.AppendLine(AddTabs(form + 1) + "on_select = " + Me.on_select + ",")
        sb.AppendLine(AddTabs(form + 1) + "on_submit = " + Me.on_submit + ",")
        sb.AppendLine(AddTabs(form + 1) + "on_submit2 = " + Me.on_submit2 + ",")
        sb.AppendLine(AddTabs(form + 1) + "row_height = " + Me.row_height.ToString() + ",")
        sb.AppendLine(AddTabs(form + 1) + "icon_width = " + Me.icon_width.ToString() + ",")
        sb.AppendLine(AddTabs(form + 1) + "scroll_keys" + Me.scroll_keys.ToString() + ",")
        Dim chces As New Table()
        Me.choices.ForEach(New Action(Of LuaTable)(Sub(t As LuaTable) chces.data.Add(t.ToString())))
        sb.AppendLine(AddTabs(form + 1) + "choices = " + chces.ToString() + ",")
        sb.Append(MyBase.ToString(format, formatProvider))
        sb.AppendLine(AddTabs(form) + "},")
        If form.Equals(0) Then
            sb.AppendLine("}")
        End If
        Return sb.ToString()
    End Function
End Class
