﻿Imports System.Text
Public Class Label
    Inherits Widget
    Public text_pen As String
    Public text_dpen As String
    Public text_hpen As String
    Public disabled As String
    Public enabled As String
    Public on_click As String
    Public on_rclick As String
    Public text As String
    Public Overrides Function ToString(format As String, formatProvider As IFormatProvider) As String
        Dim sb As New StringBuilder
        Dim form As Integer
        Int16.TryParse(format, form)
        If form.Equals(0) Then
            sb.AppendLine("self:addviews{")
        End If
        sb.AppendLine(AddTabs(form) + "widgets.Label{")
        sb.AppendLine(AddTabs(form + 1) + "text_pen = " + Me.text_pen)
        sb.AppendLine(AddTabs(form + 1) + "text_dpen = " + Me.text_dpen)
        sb.AppendLine(AddTabs(form + 1) + "text_hpen = " + Me.text_hpen)
        If disabled Then
            sb.AppendLine(AddTabs(form + 1) + "disabled = " + Me.disabled + ",")
        End If
        If enabled Then
            sb.AppendLine(AddTabs(form + 1) + "enabled = " + Me.enabled + ",")
        End If
        If on_click Then
            sb.AppendLine(AddTabs(form + 1) + "on_click = " + Me.on_click + ",")
        End If
        If on_rclick Then
            sb.AppendLine(AddTabs(form + 1) + "on_rclick = " + Me.on_rclick + ",")
        End If
        If text Then
            sb.AppendLine(AddTabs(form + 1) + "text = { text = " + Me.text + "},")
        End If
        sb.AppendLine(AddTabs(form) + "},")
        If form.Equals(0) Then
            sb.AppendLine("}")
        End If
        Return sb.ToString
    End Function
End Class
