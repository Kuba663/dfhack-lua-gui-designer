﻿Imports System.Text
Public Enum ReturnType
    BOOL
    STR
    VOID
End Enum
Public Class Func
    Public Name As String
    Public params As List(Of String)
    Dim i As Byte = 0
    Dim sb As New StringBuilder
    Public ret As ReturnType
    Public Overrides Function ToString() As String
        sb.Append("function" + Name + "(")
        params.ForEach(New Action(Of String)(AddressOf ProcessParams))
        sb.Append(")")
        sb.Append(vbNewLine)
        Select Case ret
            Case ReturnType.BOOL
                sb.AppendLine(vbTab + "return false")
            Case ReturnType.STR
                sb.AppendLine(vbTab + "return 'string'")
            Case ReturnType.VOID
                sb.Append(vbNewLine)
        End Select
        sb.AppendLine("end")
        Return sb.ToString
    End Function
    Private Sub ProcessParams(s As String)
        If i.Equals(params.Count - 1) Then
            sb.Append(s)
        Else
            sb.Append(s + ",")
        End If
        i += 1
    End Sub
End Class
