﻿Imports System.Text
Public Class Table
    Public data As New List(Of String)()
    Public Overrides Function ToString() As String
        Dim sb As New StringBuilder
        sb.Append("{")
        data.ForEach(New Action(Of String)(Sub(s As String) sb.Append(s + ",")))
        sb.Append("}")
        Return sb.ToString().Remove(sb.ToString.Length - 2, 1)
    End Function
End Class
