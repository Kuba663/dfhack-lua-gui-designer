Imports NUnit.Framework
Imports DFHLUAGUID
Imports System.IO

Namespace DFHLUAGUID_Unit_test

    Public Class TableTests
        Dim arr As New Table
        <SetUp>
        Public Sub Setup()
            arr.data.Add("jesus")
            arr.data.Add("send")
            arr.data.Add("help!")
        End Sub

        <Test>
        <Author("4372696D736F6E")>
        <Category("SubElements")>
        Public Sub Test1()
            Dim expected As String = "[jesus,send,help!]"
            Using sw As New StringWriter
                Console.SetOut(sw)
                Console.Write(arr.ToString())
                Dim result As String
                result = sw.ToString().Trim()
                Assert.AreEqual(expected, result)
            End Using
        End Sub

    End Class

End Namespace